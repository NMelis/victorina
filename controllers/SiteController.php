<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\DataUser;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        // $request = Yii::$app->request->post();
        return $this->render('login');
    }

    public function actionJoing()
    {
        $use_config = json_decode(file_get_contents('../use_config/config.json'), true);

        if (empty($_COOKIE['comp']) or empty($_COOKIE['nomination']) or empty($_COOKIE['full_name']) or empty($_COOKIE['tabel_number'])){
            return $this->render('joing');
        }

        $nomination = array_flip($use_config['nomination'])[$_COOKIE['nomination']];
        $branch = array_flip($use_config['branch'])[$_COOKIE['comp']];
        $full_name = $_COOKIE['full_name'];
        $tabel_number = $_COOKIE['tabel_number'];
        $info = array(
            'full_name' => $full_name,
            'nomination'=> $nomination,
            'branch'=> $branch,
            'email'=> $email,
            'tabel_number'=> $tabel_number
        );

        if (DataUser::is_user($info)){
            return $this->render('has_already', compact('info','use_config'));
        }else{
            return $this->render('joing');
        }

    }

    public function actionSaveuser()
    {
        $request = Yii::$app->request->post(); 
        $is_user = DataUser::new_save_user($request);

    
        $use_config = json_decode(file_get_contents('../use_config/config.json'), true);
        setcookie('comp', $use_config['branch'][$request['branch']] ?? false, time()+19981998);
        setcookie('nomination', $use_config['nomination'][$request['nomination']] ?? false, time()+19981998);
        setcookie('full_name', $request['full_name'] ?? false, time()+19981998);
        setcookie('tabel_number', $request['tabel_number'] ?? false, time()+19981998);
            
    }

    public function actionAccount()
    {
        $use_config = json_decode(file_get_contents('../use_config/config.json'), true);

        if (empty($_COOKIE['comp']) or empty($_COOKIE['nomination']) or empty($_COOKIE['full_name']) or empty($_COOKIE['tabel_number'])){
            print('melis');
            return $this->render('index');
        }

        $nomination = array_flip($use_config['nomination'])[$_COOKIE['nomination']];
        $branch = array_flip($use_config['branch'])[$_COOKIE['comp']];
        $full_name = $_COOKIE['full_name'];
        $tabel_number = $_COOKIE['tabel_number'];
        $info = array(
            'full_name' => $full_name,
            'nomination'=> $nomination,
            'branch'=> $branch,
            'tabel_number'=> $tabel_number
        );

        if (DataUser::is_user($info)){
            return $this->render('has_already', compact('use_config', 'info'));
        }else{
            return $this->render('index');
        }
       
    }

    public function actionTest($var='')
    {
        return $this->render('test');
        echo DataUser::is_user(array('full_name' => 'Melis Nurlan Uulu', 'tabel_number' => '1998', 'nomination'=>'ИНЖЕНЕР'));
    }
    
}
