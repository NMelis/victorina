<?php

namespace app\models;

class DataUser extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return 'data_user';
	}

	public static function new_save_user($data)
	{
		
        $base_db = new DataUser;

        $base_db->branch = $data['branch'];
        $base_db->full_name = $data['full_name'];
        $base_db->tabel_number = $data['tabel_number'];
        $base_db->nomination = $data['nomination'];
        $base_db->email = $data['email'];
        $base_db->password = $data['password'];
        if ($base_db->save()) return 'Success';
	}

	public static function is_user($info)
	{
    	if (self::find()->where(['full_name' => $info['full_name'], 'tabel_number' => $info['tabel_number'], 'branch'=> $info['branch']])->exists()){
    		return true;
    	}else return false;
	}


}