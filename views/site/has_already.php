<?php

/* @var $this yii\web\View */
$this->title = 'Регистрация на викторину';
?>
<? include_once('../views/site/loader.php'); ?>

<link rel="stylesheet" type="text/css" href="/web/css/has_already.css">

<div class="row">
	<div class="container">
		<p class="text-center title-p">МЕТАЛЛУРГ ГОДА</p>
	</div>
</div>

<form action="">
	<div class="row">
		<div class="col-md-4">
			<p class="select-comp">ВАШЕ ПРЕДПРИЯТИЕ</p>
		</div>
		<div class="col-md-8 compony-logo">
		<?=$branch_number = ($use_config['branch'][$info['branch']])?>
			<img src="/web/img/comp-<?=$branch_number?>.png" alt="Уральская сталь" width="150px" height="50px">
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-4">
			<p>ФАМИЛИЯ ИМЯ ОТЧЕСТВО</p>
		</div>
		<div class="col-md-8">
			<p id='fio'><?=$info['full_name']?></p>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-4">
			<p>ВАШ ТАБИЛЬНЫЙ НОМЕР</p>
		</div>
		<div class="col-md-8">
			<p id='tabel-number'><?=$info['tabel_number']?></p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<p>Ваш почтовый ящик</p>
		</div>
		<div class="col-md-8">
			<p id='tabel-number'><?=$info['email']?></p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<p>УЧАСТВОВУЕТЕ В НОМИНАЦИИ</p>
		</div>
		<div class="col-md-8">
			<div class="row">
				<ul id='prof'>
					<li class="text-center"><?=$info['nomination']?></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="row">
	<div class="col-md-12 col-md-offset-4">
	<input type="submit" class='btn btn-primary btn-submit' value="Удалить данные" />
	<input type="submit" class='btn btn-primary btn-submit' value="Редактировать данные" />
	<a href="http://v.melis.com.ru"><input type="buttom" class='btn btn-primary btn-submit' value="На главную" /></a>
	
	</div>
	</div>
</form>
<div class="row">
    <div class="col-md-12">
    <p class="text-primary text-center" style="padding-top: 50px">Осталось ждать до началы викторины:</p>
        <? include_once('../views/site/timer.php') ?>
    </div>
</div>