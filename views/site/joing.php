<?php

/* @var $this yii\web\View */
$this->title = 'Регистрация на викторину';
?>
<? include_once('../views/site/loader.php'); ?>

<link rel="stylesheet" type="text/css" href="/css/joing.css">
<div class="row">
	<div class="container">
		<p class="text-center title-p">МЕТАЛЛУРГ ГОДА</p>
	</div>
</div>

<form action="" >
	<div class="row">
		<div class="col-md-4">
			<p class="select-comp">ВЫБЕРИТЕ СВОЁ ПРЕДПРИЯТИЕ</p>
		</div>
		<div class="col-md-8 compony-logo">
			<div class="plashka"></div>
			<img src="/img/comp-2.png" alt="ОЭМК" width="20%" data-branch="1" height="20%">
			<img src="/img/comp-4.png" alt="ЛГОК" width="10%" data-branch="2" height="10%">
			<img src="/img/comp-1.png" alt="Уральская сталь" data-branch="3" width="20%" height="20%">
			<img src="/img/comp-3.png" alt="МК" width="15%" data-branch="4" height="15%">
		</div>
	</div>
	<br><br>
	<div class="row">
		<div class="col-md-4">
			<p>ФАМИЛИЯ ИМЯ ОТЧЕСТВО</p>
		</div>
		<div class="col-md-8">
			<input type="text" required='required' autocomplete="off" id='fio' size="35">
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-4">
			<p>ВАШ ТАБИЛЬНЫЙ НОМЕР</p>
		</div>
		<div class="col-md-8">
			<input type="text" required='required' autocomplete="off" id='tabel-number'  size="35">
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-4">
			<p>ВАШ ПОЧТОВЫЙ ЯЩИК</p>
		</div>
		<div class="col-md-8">
			<input type="email" required='required' autocomplete="off" id='email'  size="35">
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-4">
			<p>ПАРОЛЬ ДЛЯ ВХОДА</p>
		</div>
		<div class="col-md-8">
			<input type="text" autocomplete="off" id="password" autocomplete="off" required='required' id='fio' size="35">
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<p>УЧАСТВОВТЬ В НОМИНАЦИИ</p>
		</div>
		<div class="col-md-8">
			<div class="row">
				<ul id='prof'>
					<li class="text-center">СТАЛEВАР</li>
					<li class="text-center">ЗАЛИВЩИК МЕТАЛЛА</li>
					<li class="text-center">ПРОКАТЧИК МЕТАЛЛА</li>
					<li class="text-center">МАШИНИСТ ОКОМКОВАТЕЛЯ</li>
					<li class="text-center">ГОРНОВОЙ</li>
					<li class="text-center">ГАЗОВЩИК</li>
					<li class="text-center">ИНЖЕНЕР</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="row">
	<div class="col-md-12 col-md-offset-6">
	<input type="submit" class='btn btn-primary btn-submit' value="Зарегистрироваться" />
	<in
	</div>
	</div>
</form>


<script type="text/javascript">













var comp = get_cookie('comp');
var prof = get_cookie('prof');
var fio = get_cookie('fio');
var tabel_number = get_cookie('tabel-number');
var email = get_cookie('email');
var password = '';
function get_cookie ( cookie_name )
{
  var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
 
  if ( results )
    return ( unescape ( results[2] ) );
  else
    return null;
}

if (comp != null){
	$('[alt="'+ comp +'"]').css({
			"border" : "1px solid aqua",
	});
}
if (prof != null){
	$('#prof > li:contains("'+ prof +'")').css({
			"border" : "1px solid aqua",
		});
}

if (fio != null){
	$('#fio').val(fio)
}
if (email != null){
	$('#email').val(fio)
}

if (tabel_number != null){
	$('#tabel-number').val(tabel_number)
}


$(document).ready(function(){
	if (Number.isInteger(Number($('#tabel-number').val())) == false){
		$(this).val('');
	}

	


	$('input:submit').click(function(){
		if ($('#fio').val() != ''){
			fio = $('#fio').val();
		}
		if ($('#email').val() != ''){
			email = $('#email').val();
		}
		if ($('#password').val() != ''){
			password = $('#password').val();
		}
		if ($('#tabel-number').val() != ''){
			tabel_number = $('#tabel-number').val();
		}
		if (comp != null){
			document.cookie = "comp="+escape(comp)+"; expires=13/06/2021 00:00:00;";
		}
		if (prof != null){
			document.cookie = "prof="+escape(prof)+"; expires=13/06/2021 00:00:00;";
		}
		if (fio != null){
			document.cookie = "fio="+escape(fio)+"; expires=13/06/2021 00:00:00;";
		}
		if (email != null){
			document.cookie = "email="+escape(email)+"; expires=13/06/2021 00:00:00;";
		}
		if (tabel_number != null){
			document.cookie = "tabel_number="+escape(tabel_number)+"; expires=13/06/2021 00:00:00;";
		}
		if (comp != null && email !=null &&fio != null && tabel_number != null && prof != null && password != ''){
			$('body').html('Вы успешно записались');
			$.post({
				url: "/web/saveuser",
				data: {"branch": comp, "email": email, "password": password, "nomination": prof, "full_name": fio, "tabel_number": tabel_number},
				
				success: function(request) {
					var delay = 5;
					
					setTimeout("document.location.href='http://v.melis.com.ru/web/account'", delay);
						
				}
			});
		}
	});

	$('#prof > li').click(function() {
		$('#prof > li:contains("'+ prof +'")').css({
			"border" : "none",
		});
		prof = $(this).text();
		$('#prof > li:contains("'+ prof +'")').css({
			"border" : "1px solid aqua",
		});
	});



	$('.compony-logo > img').click(function () {
		$('[alt="'+ comp +'"]').css({
			"border" : "none",
		})
		comp = $(this).attr('alt');
		$('[alt="'+ comp +'"]').css({
			"border" : "1px solid aqua",
		});
	});

	 $("#tabel-number").keydown(function(event) {
        // Разрешаем: backspace, delete, tab и escape
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || 
             // Разрешаем: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Разрешаем: home, end, влево, вправо
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // Ничего не делаем
                 return;
        }
        else {
            // Обеждаемся, что это цифра, и останавливаем событие keypress
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
    });

});


</script>>